(function ($) {
  const socket = io('/notifications');
  const socketIdWrapper = $('.socket-id');
  const userIdWrapper = $('.user-id');
  const messageInput = $('#m');
  const messageBox = $('#messages');
  const destinationSelect = $('#destination');
  const body = $('body');

  // Generate a random number to emulate User ID
  const userId = Math.floor(Math.random()*(9999-1000+1)+1000);

  // Connect
  socket.on('connect', function () {
    socketIdWrapper.html(socket.id);
    userIdWrapper.html(userId);

    socket.emit('logged in', { userId });
  });

  $('form').submit(function(){
    // console.log(messageInput.val());
    socket.emit('chat message', { message: messageInput.val(), destination: destinationSelect.val() });
    messageInput.val('');

    return false;
  });

  // Received Message
  socket.on('chat message', function(message){
    const from = message.from === userId ? 'me' : message.from;

    messageBox.append($('<li><b>' + from + ':</b> ' + message.message + '</li>'));
    window.scrollTo(0, document.body.scrollHeight);

    // Trigger notification
    triggerNotification(message.message);
  });

  // Received User List
  socket.on('send clients', function(data){
    // console.log('Clients', data);
    destinationSelect.html('');
    destinationSelect.append($('<option value="">Todos</option>'));

    for (let index in data.usersAvailable) {
      if (data.usersAvailable.hasOwnProperty(index)) {
        let item = data.usersAvailable[index];
        if (socket.id !== index) {
          destinationSelect.append($('<option value="' + index + '">' + item + '</option>'));
        }
      }
    }
  });

  /**
   * Notification Browser
   * **/
  // Request Permissions
  if (!('Notification' in window)) {
    console.log('This browser does not support system notifications');
  } else {
    Notification.requestPermission(permission => permission);
  }

  const triggerNotification = message => {
    if (('Notification' in window)
      && Notification.permission === 'granted'
      && body.hasClass('hidden')
    ) {
      let notification = new Notification(message);
    }
  };

})(jQuery);
