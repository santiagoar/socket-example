const express = require('express');
const app = express();
const http = require('http').Server(app);
const io = require('socket.io')(http);
const port = process.env.PORT || 3005;
const cors = require('cors');
const compression = require('compression');
const bodyParser = require('body-parser');
const {get, findKey}  = require('lodash');

app.use(cors());
app.use(compression());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

// Routing
app.use(express.static(__dirname + '/public'));

app.get('/', function(req, res){
  res.sendFile(__dirname + '/public/index.html');
});

// Message history
const messagesHistory = [];

// Users
const usersAvailable = {};

const namespace = '/notifications';
const nsp = io.of(namespace);
const rootNsp = io.of('/');

// Socket.io
nsp.on('connection', socket => {

  // Disconnect
  socket.on('disconnect', () => {
    console.log('Disconect: ' + socket.id);

    delete usersAvailable[socket.id];
    nsp.clients((error, clients) => {
      if (error) throw error;
      nsp.emit('send clients', { destination: clients, usersAvailable });
    });
  });

  // Send Message
  socket.on('chat message', msg => {
    const from = usersAvailable[socket.id];
    const message = { ...msg, from };

    messagesHistory.push(message);

    if (msg.destination) {
      nsp.to(socket.id).to(msg.destination).emit('chat message', message);
    } else {
      nsp.emit('chat message', message);
    }
    console.log('chat message:', message);
  });

  socket.on('logged in', data => {
    usersAvailable[socket.id] = data.userId;

    nsp.clients((error, clients) => {
      if (error) throw error;
      console.log('------> ' + namespace, clients, usersAvailable);

      nsp.emit('send clients', { destination: clients, usersAvailable });
    });
  });

});

/** Notification Post
 * Use: POST /notifications
 *      body: { "userId": <USER-ID>, "message": "Your message" }
* */
app.post('/notifications', (req, res) => {
  const userId = get(req.body, 'userId');
  const message = get(req.body, 'message');

  const item = findKey(usersAvailable, elem => elem === Number.parseInt(userId));

  if (item) {
    nsp.to(item).emit('chat message', {message, from: 'External Message'});
    res.status(200).json({ message: 'success' });
  } else {
    res.status(422).json({ message: 'No user available.' });
  }
});

http.listen(port, function(){
  console.log('listening on *:' + port);
});
