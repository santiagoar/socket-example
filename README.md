# Socket.io Example

## Installation & run
- Clone the repo ```$ git clone <repo-name>```
- Run ```$ npm install```
- Run ```$ npm start``` to start, or ```$ npm run debug``` to start in debug mode.
- Go to `http://localhost:3005` in many browsers tabs.
- Enjoy :P
